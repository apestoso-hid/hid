##
# Teclado
#
# @file
# @version 0.1

.PHONY: watch
watch:
	@echo "Observando cambios en archivos python"
	@watchexec -d 1000 -p -e py ./install-into-board.sh

.PHONY: install
install:
	@./install-into-board.sh

# end
