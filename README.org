#+TITLE: Teclado - Apestoso HID

Código que implementa HID en el microcontrolador.

** Configuración
El comportamiento puede cambiar configurando un archivo =conf.json= en =CIRCUITPY=:

#+begin_src json
{ "teclas": [20, 8, 25], "repeticion": { "espera": 0.250, "intervalo": 0.03 } }
#+end_src

*** =teclas=
- El indice 0 corresponde al pedal derecho.
- El indice 1 corresponde al pedal izquierdo.
- El indice 2 corresponde al boton.
*** =repeticion=
- =espera= corresponde a la cantidad de tiempo en segundos que el microcontrolador debe esperar antes de enviar la siguiente repeticion al presionarse una tecla.
- =intervalo= indica la cantidad de tiempo en segundos que una nueva repeticion es emitida luego de haberse enviado la primera.

Este comportamiento emula la repeticion de teclas en un teclado 100% real segun las reglas del sistema operativo.
