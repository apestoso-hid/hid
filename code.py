"""
HID basico

codigo copiado de https://learn.adafruit.com/introducing-adafruit-itsybitsy-m4/circuitpython-hid-keyboard-and-mouse
y modificado para mis necesidades
"""
import time
import json

import board
import digitalio
import usb_hid
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from adafruit_hid.keycode import Keycode

# los pins a usar, cada uno tendra un pullup interno
keypress_pins = [board.A1, board.A2, board.A3]
# arreglo de objetos key
key_pin_array = []
# letras ordenadas por pin que se enviaran
conf = {"teclas": [], "repeticion": {"espera": 0.250, "intervalo": 0.03}}

# carga configuracion de teclas
try:
    with open("./conf.json") as r:
        conf = json.load(r)
except FileNotFoundError:
    # FIXME: Hacer algo?
    pass

# The keyboard object!
time.sleep(1)  # duerme por 1 segundo para evitar condiciones de carrera
keyboard = Keyboard(usb_hid.devices)
keyboard_layout = KeyboardLayoutUS(keyboard)

# Make all pin objects inputs with pullups
for pin in keypress_pins:
    key_pin = digitalio.DigitalInOut(pin)
    key_pin.direction = digitalio.Direction.INPUT
    key_pin.pull = digitalio.Pull.UP
    key_pin_array.append(key_pin)

previousKey = None

while True:
    # revisa cada pin
    for key_pin in key_pin_array:
        i = key_pin_array.index(key_pin)
        if not key_pin.value:
            try:
                key = conf["teclas"][i]
                keyboard.press(key)
                keyboard.release_all()
                if previousKey != key:
                    # espera larga como en un teclado real
                    previousKey = key
                    time.sleep(conf["repeticion"]["espera"])
                else:
                    # espera corta como en un teclado real
                    time.sleep(conf["repeticion"]["intervalo"])
            except IndexError:
                print("pines no coinciden con la cantidad de teclas asignadas")
        else:
            # reinicia la banderilla solo si estamos revisando el mismo pin de
            # un ciclo anterior
            key = conf["teclas"][i]
            if key == previousKey:
                previousKey = None

    time.sleep(0.01)
