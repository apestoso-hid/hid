#!/usr/bin/env bash
set -euo pipefail

if [[ -d /run/media/${USER}/CIRCUITPY ]]; then
    if [[ ! -f /run/media/${USER}/CIRCUITPY/conf.json ]]; then
        cp ./boot.py ./code.py ./conf.json /run/media/${USER}/CIRCUITPY/
        echo "copiado boot.py, code.py y conf.json a 'CIRCUITPY'"
    else
        cp ./boot.py ./code.py /run/media/${USER}/CIRCUITPY/
        echo "copiado boot.py y code.py a 'CIRCUITPY'"
    fi
else
    echo "[WARN] Directorio '/run/media/${USER}/CIRCUITPY' no existe!"
fi
